var express = require('express')
  , http = require('http')
  , path = require('path');

var session = require('express-session');
var morgan = require("morgan");
var mongoose = require("mongoose");
var moment = require('moment');
var config = require("./config");
var setupController = require("./api/controllers/setupController");
var nguoidungController = require("./api/controllers/nguoidungController");
var deviceController = require("./api/controllers/deviceController");


var app = express();

//upload
const fileUpload = require('express-fileupload');

app.use(fileUpload());
app.post('/upload', function (req, res) {
  if (Object.keys(req.files).length == 0) {
    return res.status(400).send('No files were uploaded.');
  }
  let sampleFile = req.files.file;
  sampleFile.mv(__dirname + '/public/upload/data/' + sampleFile.name, function (err) {
    if (err)
      return res.status(500).send(err);

    res.send('File uploaded!');
  });

});
//end upload

var bodyParser = require("body-parser");
// all environments
app.use(morgan("dev"));
app.set('port', process.env.PORT || 1456);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000 }
}))

// development only

//db connect
mongoose.connect(config.getDbConnectionString());
setupController(app);
nguoidungController(app);
deviceController(app);


app.get("/", function (req, res) {

  var user = req.session.user,
    username = req.session.username;
  if (user == null) {
    res.redirect("/login");
    return;
  }
  else
    res.render("devices.ejs", { user: user, username: username });

})

app.get("/login", function (req, res) {

  var user = req.session.user,
    username = req.session.username;
  res.render("login", { user: user, username: username });


})

app.get("/devices", function (req, res) {

  var user = req.session.user,
    username = req.session.username;

  if (user == null) {
    res.redirect("/login");
    return;
  }
  else
    res.render("devices", { user: user, username: username });
})

app.get("/logout", function (req, res) {

  req.session.destroy(function (err) {
    res.redirect("/");
  })

})

app.listen(1454)
