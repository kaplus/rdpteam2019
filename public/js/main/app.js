// angular
var app = angular.module("app.todos", ["xeditable", "tabs", "toaster", "ngSanitize", "ui.select", "angularUtils.directives.dirPagination", "angularFileUpload"]);

//filter combobox
app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            out = items;
        }

        return out;
    };
});


// đăng nhập
app.controller("dangnhapController", ['$scope', 'svDangnhaps', 'toaster', function ($scope, svDangnhaps, toaster) {

    $scope.formData = {};
    $scope.loading = true;
    $scope.dangnhap = function () {
        var nguoidung = {
            user: $scope.formData.user,
            pass: $scope.formData.pass
        }
        svDangnhaps.dangnhap(nguoidung).then(function (response) {

            if (response.data.length == 0)
                toaster.pop('error', "Đăng nhập", "Tài khoản hoặc mật khẩu chưa đúng", 5000, 'trustedHtml');
            else {

                toaster.pop('success', "Đăng nhập", "Đăng nhập thành công", 5000, 'trustedHtml');
                location.href = "/"
            }
        })
    }
}]);

// devices
app.controller("deviceController", ['$scope', 'svDevices', 'toaster', function ($scope, svDevices, toaster) {

    $scope.appName = "Devices";
    $scope.formData = {};
    $scope.loading = true;
    $scope.ketquas = [];
    $scope.ketqua = [];
    $scope.parseInt = parseInt;
    $scope.totalItems = 0;
    $scope.page = 1;
    $scope.event = "";

    //load data from api
    count();
    getResultsPage(1);

    function count() {
        svDevices.count().then(function (response) {
            $scope.totalItems = response.data;
        });
    }

    function reset() {
        $scope.formData.ip = "";
        $scope.formData.phone = "";
        $scope.formData.address = "";
        $scope.formData.gateway = "";
        $scope.formData.gateway4G = "";
        $scope.formData.imei = "";
        $scope.formData.port = "";
        $scope.formData.port4G = "";
        $scope.formData.responsetime = "";
    }

    $scope.pageChanged = function (newPage) {
        getResultsPage(newPage);
    };

    function getResultsPage(pageNumber) {
        $scope.loading = true;
        svDevices.limit(pageNumber).then(function (response) {
            $scope.ketquas = response.data;
            // $scope.totalItems = response.data.length; 
            $scope.page = pageNumber;
            $scope.loading = false;

        });

    }
    $scope.add = function () {
        $scope.event = "add";
        reset();
    }

    function create() {

        //last
        svDevices.last().then(function (response) {

            var seed = {
                _id: response.data + 1, //last id
                ip: $scope.formData.ip,
                phone: $scope.formData.phone,
                address: $scope.formData.address,
                gateway: $scope.formData.gateway,
                gateway4G: $scope.formData.gateway4G,
                imei: $scope.formData.imei,
                port: $scope.formData.port,
                port4G: $scope.formData.port4G,
                responsetime: $scope.formData.responsetime,
                status: true
            }
            //tạo mới
            $scope.loading = true;

            svDevices.create(seed).then(function (response) {

                $scope.ketquas = response.data;
                $scope.totalItems = response.data.length;
                reset();
                $scope.loading = false;
                toaster.pop('success', "Thông báo", "Tạo mới thành công", 5000, 'trustedHtml');
            })
            //end tạo mới

        });

    }
    $scope.search = function () {
        var keyword = $scope.formData.text;
        if (keyword == "") {
            count();
            getResultsPage(1);
        } else {
            $scope.loading = true;
            svDevices.search(keyword).then(function (response) {
                $scope.ketquas = response.data;
                $scope.totalItems = response.data.length;
                $scope.loading = false;
            })
        }

    }
    $scope.detail = function (id) {
        $scope.loading = true;
        svDevices.detail(id).then(function (response) {
            $scope.ketqua = response.data;
            $scope.loading = false;
        });
    }

    function update(id) {

        var seed = {
            _id: id,
            ip: $scope.formData.ip,
            phone: $scope.formData.phone,
            address: $scope.formData.address,
            gateway: $scope.formData.gateway,
            gateway4G: $scope.formData.gateway4G,
            imei: $scope.formData.imei,
            port: $scope.formData.port,
            port4G: $scope.formData.port4G,
            responsetime: $scope.formData.responsetime,
            status: true

        }
        $scope.loading = true;
        svDevices.update(seed).then(function (response) {
            $scope.ketquas = response.data;
            $scope.totalItems = response.data.length;
            $scope.loading = false;
            toaster.pop('success', "Thông báo", "Cập nhật thành công", 5000, 'trustedHtml');

        })
    }

    $scope.biding = function (doituong) {

        $scope.event = "edit";
        console.log(doituong);
        $scope.formData.ip = doituong.ip;
        $scope.formData.phone = doituong.phone;
        $scope.formData.address = doituong.address;
        $scope.formData.gateway = doituong.gateway;
        $scope.formData.gateway4G = doituong.gateway4G;
        $scope.formData.imei = doituong.imei;
        $scope.formData.port = doituong.port;
        $scope.formData.port4G = doituong.port4G;
        $scope.formData.responsetime = doituong.responsetime;
    }

    $scope.save = function () {
        if ($scope.event == "add") create();
        else
            update($scope.ketqua[0]._id);
    }
    $scope.delete = function (doituong) {
        $scope.loading = true;
        svDevices.delete(doituong).then(function (response) {
            $scope.ketquas = response.data;
            $scope.totalItems = response.data.length;
            $scope.loading = false;
            toaster.pop('success', "Thông báo", "Xóa thành công", 5000, 'trustedHtml');
        })
    }
}]);