
var app = angular.module("app.todos");

app.factory("svDevices", ["$http", function ($http) {

    return {
        get: function () {
            return $http.get("/api/devices");
        },
        count: function () {
            return $http.get("/api/countdevices");
        },
        create: function (atmgData) {

            return $http.post("/api/createdevice", atmgData);
        },
        update: function (data) {
            return $http.put("/api/updatedevice", data);
        },

        search: function (id) {
            return $http.get("/api/searchdevices/" + id);
        },

        limit: function (id) {
            return $http.get("/api/limitdevices/" + id);
        },
        last: function (id) {
            return $http.get("/api/lastdevice");
        },

        detail: function (id) {
            return $http.get("/api/device/" + id);
        },
        // delete: function(id){
        //   return $http.delete("/api/nguoidung/"+ id);
        // },
        delete: function (data) {
            return $http.put("/api/deletedevice", data);
        },

    }
}]);


app.factory("svDangnhaps", ["$http", function ($http) {

    return {
        dangnhap: function (data) {
            return $http.post("/api/dangnhap", data);
        }

    }
}]);