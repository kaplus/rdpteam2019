// Mục đích của controller này là tạo ra các Restfull API
var Devives = require("../models/deviceModel");

function getDevives(res) {
    Devives.aggregate([

        {
            $match: {
                status: true
            }
        },
        {
            $sort: {
                _id: -1
            }
        }
    ], function(err, ketquas) {
        if (err) throw err;
        res.json(ketquas);
    })
}

module.exports = function(app) {

    //get all todos
    app.get("/api/devives", function(req, res) {
        getDevives(res);
    })

    app.get("/api/lastdevice", function(req, res) {

        Devives.aggregate([{
                $sort: {
                    _id: -1
                }
            },
            {
                $limit: 1
            }
        ], function(err, ketquas) {
            if (err) throw err;
            res.json(ketquas[0]._id);
        })

    })


    app.get('/api/countdevices', function(req, res) {

        Devives.aggregate([

            {
                $match: {
                    status: true
                }
            }

        ], function(err, ketquas) {
            if (err) throw err;
            res.json(ketquas.length);
        })

    });

    app.get("/api/searchdevices/:id", function(req, res) {

        Devives.aggregate([

            {
                $match: {
                    status: true,
                    "$or": [{
                        "_id": (parseInt(req.params.id))
                    }, {
                        "ip": req.params.id
                    }, {
                        "phone": req.params.id
                    }, {
                        "imei": req.params.id
                    }]
                }
            },
            {
                $sort: {
                    _id: -1
                }
            }

        ], function(err, ketquas) {
            if (err) throw err;
            res.json(ketquas);
        })

    })

    app.get("/api/limitdevices/:id", function(req, res) {


        Devives.aggregate([

            {
                $match: {
                    status: true
                }
            },
            {
                $sort: {
                    _id: -1
                }
            },
            {
                $skip: (parseInt(req.params.id) - 1) * 10
            }, // req.params.id = trang
            {
                $limit: 10
            }


        ], function(err, ketquas) {
            if (err) throw err;
            res.json(ketquas);
        });

    })
    //tạo mới
    app.post("/api/createdevice", function(req, res) {

        var seed = {

            _id: req.body._id,
            ip: req.body.ip,
            phone: req.body.phone,
            address: req.body.address,
            gateway: req.body.gateway,
            gateway4G: req.body.gateway4G,
            imei: req.body.imei,
            port: req.body.port,
            port4G: req.body.port4G,
            responsetime: req.body.responsetime,
            status: true
        };



        Devives.create(seed, function(err, seed) {
            if (err) {
                throw err;
            } else {
                getDevives(res);
            }
        });


    });
    //chi tiết
    app.get("/api/device/:id", function(req, res) {

        Devives.aggregate([{
                $lookup: {
                    from: 'chinhanhs',
                    localField: 'chinhanh',
                    foreignField: '_id',
                    as: 'thongtinchinhanh'
                },

            },
            {
                $lookup: {
                    from: 'chucdanhs',
                    localField: 'chucdanh',
                    foreignField: '_id',
                    as: 'thongtinchucdanh'
                },

            },
            {

                $match: {
                    _id: parseInt(req.params.id)
                }
            }


        ], function(err, ketquas) {
            if (err) throw err;
            res.json(ketquas);
        })

    })
    //delete - thuc chat la update
    app.put("/api/deletedevice", function(req, res) {



        if (!req.body._id) {
            return res.status(500).send("ID is required");
        } else {
            Devives.update({
                _id: req.body._id
            }, {
                status: false,

            }, function(err, atmg) {
                if (err) {
                    return res.status(500).json(err);
                } else {
                    getDevives(res);
                }
            })
        }

    });
    //update
    app.put("/api/updatedevice", function(req, res) {

        //update ko hinh anh
        if (!req.body._id) {
            return res.status(500).send("ID is required");
        } else {
            Devives.update({
                _id: req.body._id
            }, {

                ip: req.body.ip,
                phone: req.body.phone,
                address: req.body.address,
                gateway: req.body.gateway,
                gateway4G: req.body.gateway4G,
                imei: req.body.imei,
                port: req.body.port,
                port4G: req.body.port4G,
                responsetime: req.body.responsetime


            }, function(err, atmg) {
                if (err) {
                    return res.status(500).json(err);
                } else {
                    getDevives(res);
                }
            })
        }
        //



    });


    app.post("/api/dangnhap", function(req, res) {

        var data = {
            username: req.body.user,
            matkhau: req.body.pass
        };

        Devives.aggregate([{
                $lookup: {
                    from: 'chinhanhs',
                    localField: 'chinhanh',
                    foreignField: '_id',
                    as: 'thongtinchinhanh'
                },

            },
            {
                $lookup: {
                    from: 'chucdanhs',
                    localField: 'chucdanh',
                    foreignField: '_id',
                    as: 'thongtinchucdanh'
                },

            },
            {
                $match: {
                    "$and": [{
                        "username": data.username
                    }, {
                        "matkhau": data.matkhau
                    }]
                }
            }
        ], function(err, ketqua) {
            if (err) res.status(500).json(err);
            else {
                if (ketqua.length == 0) {
                    console.log("Đăng nhập thất bại");
                    res.json(ketqua);
                } else {

                    //log dang nhap
                    Devives.update({
                        _id: ketqua[0]._id
                    }, {
                        logdangnhap: Date.now()

                    }, function(err, nguoidung) {
                        if (err) {
                            return res.status(500).json(err);
                        } else {
                            //
                            req.session.username = ketqua[0].tennguoidung;
                            req.session.user = ketqua[0];
                            res.json(ketqua[0]);
                            //
                        }
                    })
                }

            }

        })

    });

}