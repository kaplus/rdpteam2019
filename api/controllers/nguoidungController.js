// Mục đích của controller này là tạo ra các Restfull API
var Nguoidungs = require("../models/nguoidungModel");

module.exports = function (app) {

    app.post("/api/dangnhap", function (req, res) {

        var data = {
            username: req.body.user,
            matkhau: req.body.pass
        };

        Nguoidungs.aggregate([

            {
                $match: {
                    "$and": [{
                        "username": data.username
                    }, {
                        "matkhau": data.matkhau
                    }]
                }
            }
        ], function (err, ketqua) {
            if (err) res.status(500).json(err);
            else {
                if (ketqua.length == 0) {
                    console.log("Đăng nhập thất bại");
                    res.json(ketqua);
                } else {

                    //log dang nhap
                    Nguoidungs.update({
                        _id: ketqua[0]._id
                    }, {
                            logdangnhap: Date.now()

                        }, function (err, nguoidung) {
                            if (err) {
                                return res.status(500).json(err);
                            } else {
                                //
                                req.session.username = ketqua[0].tennguoidung;
                                req.session.user = ketqua[0];
                                res.json(ketqua[0]);
                                //
                            }
                        })
                }

            }

        })
    });

}