
var Nguoidungs = require("../models/nguoidungModel");
var Devives = require("../models/deviceModel");


module.exports = function (app) {
  
    app.get("/api/setupNguoidungs", function (req, res) {

        //setup seed data
        var seeds = [
            {
                _id: 1,
                tennguoidung: "Đặng Quốc Bảo",
                sodienthoai: "0937848000",
                email: "quocbao@gmail.com",
                username: "123",
                matkhau: "123",
                hoatdong: true,
                chinhanh: 1,
                chucdanh: 1
            },
            {
                _id: 2,
                tennguoidung: "Phi Nhung Phạm",
                sodienthoai: "0937848000",
                email: "phinhung@gmail.com",
                username: "phinhung",
                matkhau: "123456",
                hoatdong: true,
                chinhanh: 2,
                chucdanh: 2
            },
            {
                _id: 3,
                tennguoidung: "Đặng Thị Mai Sa",
                sodienthoai: "0937848000",
                email: "maisadang@gmail.com",
                username: "maisa",
                matkhau: "123456",
                hoatdong: true,
                chinhanh: 2,
                chucdanh: 3
            },
            {
                _id: 4,
                tennguoidung: "Nguyễn Thị Mai Linh",
                sodienthoai: "0937848000",
                email: "mailinh@gmail.com",
                username: "mailinh",
                matkhau: "123456",
                hoatdong: true,
                chinhanh: 1,
                chucdanh: 4
            },
            {
                _id: 5,
                tennguoidung: "Đặng Thanh Qui",
                sodienthoai: "0937848000",
                email: "thanhqui@gmail.com",
                username: "thanhqui",
                matkhau: "123456",
                hoatdong: true,
                chinhanh: 1,
                chucdanh: 5
            }


        ];

        Nguoidungs.create(seeds, function (err, results) {
            res.send(results);
        });

    })

    app.get("/api/setupDevices", function (req, res) {

        //setup seed data
        var seeds = [
            {
                _id:25,
                ip :"178.1.22.16",
                phone :"0934673287",
                address: "Số 10 Đường 5B, Phường Bình Trị Đông B, Quận Bình Tân, TP Hồ Chí Minh",
                gateway : "192.168.99.45",
                port: 7655,
                responsetime: 3000,
                imei: "dafasfasf4524csv",
                gateway4G: "123.23.23.11",
                port4G: 3434,
                status: true
            },
            {
                _id:26,
                ip :"178.1.22.17",
                phone :"0947483847",
                address: "36/15, khu phố Tân Hòa, Phường Đông Hòa, TX Dĩ An, Bình Dương",
                gateway : "192.168.99.45",
                port: 7655,
                responsetime: 3000,
                imei: "dafasfasf4524csv",
                gateway4G: "123.23.23.11",
                port4G: 3434,
                status: true
            },
            {
                _id:27,
                ip :"178.1.22.18",
                phone :"0946234537",
                address: "766A/4 Lạc Long Quân, Phường 9, Quận Tân Bình, TP Hồ Chí Minh",
                gateway : "192.168.99.45",
                port: 7655,
                responsetime: 3000,
                imei: "dafasfasf4524csv",
                gateway4G: "123.23.23.11",
                port4G: 3434,
                status: true
            },
            {
                _id:28,
                ip :"178.1.22.19",
                phone :"0936462635",
                address: "Lô 18 đường A4, Cụm công nghiệp Bắc Duyên Hải, Phường Duyên Hải, Thành phố Lào Cai, Lào Cai",
                gateway : "192.168.99.45",
                port: 7655,
                responsetime: 3000,
                imei: "dafasfasf4524csv",
                gateway4G: "123.23.23.11",
                port4G: 3434,
                status: true
            }
            


        ];

        Devives.create(seeds, function (err, results) {
            res.send(results);
        });

    })

}

