var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var deviceSchema = new Schema({ // giống như cấu trúc bảng

  _id: Number,
  ip: String,
  phone: String,
  address: String,
  gateway: String,
  port: Number,
  responsetime: Number,
  imei: String,
  gateway4G: String,
  port4G: Number,
  status: Boolean


});

var Devices = mongoose.model("Device", deviceSchema); // trả về nó một cái model: giống như tên bảng là Todo1, và cấu trúc bảng là todoSchema.

module.exports = Devices;