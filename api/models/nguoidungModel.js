var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var nguoidungSchema = new Schema({ // giống như cấu trúc bảng

  _id: Number,
  tennguoidung: String,
  sodienthoai: String,
  email: String,
  username: String,
  matkhau: String,
  hoatdong: Boolean,
  chinhanh: Number,
  chucdanh: Number,
  logdangnhap: Date,
  hinhanh: String

});

var Nguoidungs = mongoose.model("Nguoidung", nguoidungSchema); // trả về nó một cái model: giống như tên bảng là Todo1, và cấu trúc bảng là todoSchema.

module.exports = Nguoidungs;